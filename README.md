
<!-- README.md is generated from README.Rmd. Please edit that file -->

# coverR

<!-- badges: start -->
<!-- badges: end -->

<img src="inst/extdata/coveR-logo_HR.png" width="200"/>

Package `coveR` allows to import, classify and analyze Digital Cover
Photography (DCP) images of tree canopies and export forest canopy
attributes like Foliage Cover and Leaf Area Index (LAI). 

The DCP method was pioneered by (Macfarlane et al. 2007a), and it is
based in acquiring upward-looking images of tree crowns using a *normal*
lens, namely a lens with a restricted (typically 30°) field of view
(FOV), although larger FOV images (e.g. those from camera traps or
smartphones) can be considered.  

The process of analyzing these images is substantially:  
1) import a cover image;  
2) create a binary image of canopy (0) and gaps (1);  
3) further classify gaps based on their size (large and small gaps);  
4) apply theoretical gap formulas to relate canopy structure to gap
fraction.  

The article describing the methodology is available at (Chianucci,
Ferrara, and Puletti 2022).

# Installation

You can install the *development* version of coveR from
[GitLab](https://gitlab.com/fchianucci/coveR):

``` r
# install.packages("devtools")
devtools::install_gitlab("fchianucci/coveR")
```

# Workflow

The basic steps of processing cover images are:

1.  import an image
2.  create a binary classification of gaps and canopy
3.  label each gap
4.  classify gaps based on their size
5.  retrieve canopy attributes from gap size

All these steps are performed by a single function `coveR()`. The
following sections illustrate step-by-step the whole workflow required
to retrieve canopy attributes from DCP images.

## 1) Open an image and select the blue-channel.

First we need to import an RGB image:

``` r
library(coveR)
image <- system.file('extdata','IMG1.JPG',package='coveR')
```

<img src="man/figures/README-input-1.png" width="50%" style="display: block; margin: auto;" />

The first step of the `coveR` function allows to import the image as a
single-channel raster, using `terra::rast(band=x)` functionality. The
blue channel `coveR(filename, channel=3`) is generally preferred as it
enables highest contrast between sky and canopy pixels, easing image
classification. This is well illustrated in the following figure:

<img src="man/figures/README-channels-1.png" width="60%" style="display: block; margin: auto;" />

We can then take the blue channel image using the `channel=3` argument:

<img src="man/figures/README-displayblue-1.png" width="60%" style="display: block; margin: auto;" />

The extra-argument `exif` allows to collect image metadata such as
Camera model, Acquisition time, image and file size. It uses native R
functionality from the `EXIFr` package
(<https://github.com/cmartin/EXIFr>) avoiding the need to install
third-party solutions.

In addition, the extra-argument `crop` allows to specify if some
horizontal lines should be removed from the bottom side of the image.
This option is useful when removing the *timestamp* from camera trap
images. These data are useful when the method is applied to continuous
cameras, such as camera traps, see:
<https://www.sciencedirect.com/science/article/pii/S0168192321002008>.

## 2) Classify and create a binary raster image.

Once imported, the functions uses the `thdmethod` argument to classify
the blue-channel pixels to get a binary image of sky (1) and canopy (0)
pixels. Note that the sky (hereafter gap) pixels are the target of
subsequent analyses.

`coveR` function uses the `auto_thresh()` functionality of the
`autothresholdr` package to define an image thresholding. The default
thresholding function used by `thd_blue` is ‘Otsu’. For other methods,
see: <https://imagej.net/plugins/auto-threshold>

After this steps, a single channel binary (0,1) SpatRaster is created,
which can be inspected with the `display` extra-argument:

<img src="man/figures/README-thd_blue-1.png" width="60%" style="display: block; margin: auto;" />

## 3) Segment and create labelled gaps image.

Retrieving canopy attributes requires further classifying gap pixels
(those labelled as 1 in the binary raster image) as large,
between-crowns gaps and small, within-crown gaps. The function use the
`ConnCompLabel` functionality of `mcg` package
(<https://cran.r-project.org/web/packages/mgc/index.html>) to assign a
numeric label to each distinct gap.

The function returns a single-channel raster image with each gap with a
numeric unique label:

<img src="man/figures/README-labelgaps-1.png" width="60%" style="display: block; margin: auto;" />

Once labelled, each gaps can be classified based on their size.

## 4) Classify gaps based on their size.

There are basically two methods to classify gaps based on their (pixel)
size. A very effective method is the one proposed by (Macfarlane et al.
2007b), which consider large gaps (gL) those larger 1.3% of the image
area (this value can be varied in the `thd` argument). It can be
selected via the `gapmethod='macfarlane'` argument.

Alternatively, we can use the large gap method proposed by (Alivernini
et al. 2018) which is based on the statistical distribution of gap size
inside images. In this method large gaps (gL) are considers as:
$gL \ge \mu + \sqrt{{\sigma \over n}}$. Compared with the other method,
this is canopy-density dependent, as the large gap threshold varied with
the actual canopy considered. It can be selected via the
`gapmethod='alivernini'` argument.

    #>   Var1   Freq     id     NR        gL
    #> 1    0 432282 IMG1_3 763264    Canopy
    #> 2    1      3 IMG1_3 763264 Small_gap
    #> 3    2      1 IMG1_3 763264 Small_gap
    #> 4    3      6 IMG1_3 763264 Small_gap
    #> 5    4      4 IMG1_3 763264 Small_gap
    #> 6    5      1 IMG1_3 763264 Small_gap

The function returns a dataframe of classified pixels into ‘Canopy’,
‘Small_gap’ and ‘Large_gap’ classes. ‘Var1’\>0 identify each gap region,
‘Freq’ is the number of pixels in each gap, while ‘NR’ is the image
size.

## 5) Retrieving canopy attributes from classified gap and canopy pixels

Once we classified gaps into large and small gaps using one of the two
methods above, the `coveR` function estimates canopy attributes from the
following modified Beer-Lambert law equations (Macfarlane et al. 2007b).
The inversion for leaf area requires parametrizing an extinction
coefficient *k*, which is by default set to 0.5 (spherical leaf angle
distribution):

- Gap fraction (GF) is calculated as the fraction of gap pixels (those
  labelled as 1 in the binary image):

  $$
  GF= {gT \over NR}
  $$

  where $gT$ is the number of gap pixels, $NR$ is the total number of
  image pixels.;

- Foliage Cover (FC) is the complement of gap fraction:

  $$
  FC= 1-GF
  $$

- Crown Cover (CC) is calculated as the complement of large gap
  fraction:

  $$CC= 1-{gL \over NR}$$, where $gL$ is the number of large gap pixels,
  $NR$ is the total number of image pixels;

- Crown Porosity (CP) is calculated as the fraction of gaps within crown
  envelopes:

$$
CP=1- {FC \over CC}
$$

By knowing these canopy attributes, it is possible to derive effective
LAI (Le) as:

$$
Le= {-log(GF) \over k}
$$

- The actual LAI (L) is calculated as:

$$
L=-CC {log(CP) \over k}
$$

As the actual LAI considers clumping effects, $L \ge Le$.

- the Clumping Index (CI) is calculated as:

$$
CI = {Le \over L}
$$

    #> # A tibble: 1 × 24
    #>   path_id   id    date                  day month  year    FC    CC     CP    Le
    #>   <chr>     <chr> <dttm>              <int> <dbl> <dbl> <dbl> <dbl>  <dbl> <dbl>
    #> 1 /private… IMG1… 2018-08-21 18:39:07    21     8  2018 0.565 0.604 0.0646 0.980
    #> # ℹ 14 more variables: L <dbl>, CI <dbl>, k <dbl>, imgchannel <dbl>,
    #> #   gap_method <chr>, gap_thd <dbl>, img_method <chr>, img_thd <dbl>,
    #> #   blurriness <dbl>, BSI <dbl>, FileSize <dbl>, ImageSize <chr>, Camera <chr>,
    #> #   Model <chr>

## 6) Export the classified image

We can export the raster image with classified gap sizes using the
`export.image` argument function.

<img src="man/figures/README-canopyraster-1.png" width="60%" style="display: block; margin: auto;" />

# Quality check

- The quality of image can be inspected from three columns:

  - `FileSize`. Previous studies indicate that smaller size can indicate
    lower quality images, e.g. images lower than 2/3 the median size of
    the time-series (Chianucci, Bajocco, and Ferrara 2021; Ryu et al.
    2012). (This threshold is empirical, please check with your time
    series)

  - `blurriness`. A Laplacian variance filter is calculated from RGB
    images. Value \< 0.0075 may indicate high blurriness in the images.
    (This threshold is empirical, please check with your time series)

  - `BSI` . The blue sky index is calculated after. Value \< 0.65 may
    indicate overcast sky (Ryu et al. 2012), which is ideal condition
    for canopy image.

# More about

The functions are optimized to batch processing bunches of DCP images.
In such a case, you can use ‘traditional’ looping through images, as in
example below.

``` r
data_path<-system.file('extdata',package='coveR')
files<-dir(data_path,pattern='jpeg$|JPG$', full.names = T)

res<-NULL
for (i in 1:length(files)){
  cv<-coveR(files[i], display=F, message=F)
  res<-rbind(res,cv)
}
res
```

    #> # A tibble: 1 × 24
    #>   path_id   id    date                  day month  year    FC    CC     CP    Le
    #>   <chr>     <chr> <dttm>              <int> <dbl> <dbl> <dbl> <dbl>  <dbl> <dbl>
    #> 1 /private… IMG1… 2018-08-21 18:39:07    21     8  2018 0.565 0.604 0.0646  1.67
    #> # ℹ 14 more variables: L <dbl>, CI <dbl>, k <dbl>, imgchannel <dbl>,
    #> #   gap_method <chr>, gap_thd <dbl>, img_method <chr>, img_thd <dbl>,
    #> #   blurriness <dbl>, BSI <dbl>, FileSize <dbl>, ImageSize <chr>, Camera <chr>,
    #> #   Model <chr>

<div id="refs" class="references csl-bib-body hanging-indent"
entry-spacing="0">

<div id="ref-alivernini2018" class="csl-entry">

Alivernini, Alessandro, Silvano Fares, Carlotta Ferrara, and Francesco
Chianucci. 2018. “An Objective Image Analysis Method for Estimation of
Canopy Attributes from Digital Cover Photography.” *Trees* 32 (3):
713–23. <https://doi.org/10.1007/s00468-018-1666-3>.

</div>

<div id="ref-chianucci2021" class="csl-entry">

Chianucci, Francesco, Sofia Bajocco, and Carlotta Ferrara. 2021.
“Continuous Observations of Forest Canopy Structure Using Low-Cost
Digital Camera Traps.” *Agricultural and Forest Meteorology* 307
(September): 108516. <https://doi.org/10.1016/j.agrformet.2021.108516>.

</div>

<div id="ref-chianucci2022" class="csl-entry">

Chianucci, Francesco, Carlotta Ferrara, and Nicola Puletti. 2022.
“coveR: An R Package for Processing Digital Cover Photography Images to
Retrieve Forest Canopy Attributes.” *Trees*, August.
<https://doi.org/10.1007/s00468-022-02338-5>.

</div>

<div id="ref-macfarlane2007" class="csl-entry">

Macfarlane, Craig, Megan Hoffman, Derek Eamus, Naomi Kerp, Simon
Higginson, Ross McMurtrie, and Mark Adams. 2007b. “Estimation of Leaf
Area Index in Eucalypt Forest Using Digital Photography.” *Agricultural
and Forest Meteorology* 143 (3-4): 176–88.
<https://doi.org/10.1016/j.agrformet.2006.10.013>.

</div>

<div id="ref-macfarlane2007a" class="csl-entry">

———. 2007a. “Estimation of Leaf Area Index in Eucalypt Forest Using
Digital Photography.” *Agricultural and Forest Meteorology* 143 (3-4):
176–88. <https://doi.org/10.1016/j.agrformet.2006.10.013>.

</div>

<div id="ref-ryu2012" class="csl-entry">

Ryu, Youngryel, Joseph Verfaillie, Craig Macfarlane, Hideki Kobayashi,
Oliver Sonnentag, Rodrigo Vargas, Siyan Ma, and Dennis D. Baldocchi.
2012. “Continuous Observation of Tree Leaf Area Index at Ecosystem Scale
Using Upward-Pointing Digital Cameras.” *Remote Sensing of Environment*
126 (November): 116–25. <https://doi.org/10.1016/j.rse.2012.08.027>.

</div>

</div>
